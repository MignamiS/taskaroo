Taskaroo
========
Author: Simone Mignami <simone.mig29@gmail.com>

Description: Taskaroo is a small accessible and opensource program that stores task and ToDos.
Its features are accessible using a screen reader (such as JAWS or NVDA).

Version: beta
