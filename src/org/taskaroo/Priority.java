package org.taskaroo;

public enum Priority {
	HIGH, MEDIUM, LOW;
}
