package org.taskaroo;

public class TaskarooCommandLineClient {

	public static void main(final String[] args) {
		final ToDoList list = new ToDoList("todo test");

		// tags
		final TodoTag tagApple = new TodoTag("Apple");
		final TodoTag tagOrange = new TodoTag("Orange");

		// todo
		final ToDo t1 = new ToDo("first", Priority.HIGH);
		t1.addTag(tagOrange);
		t1.addTag(tagApple);
		final ToDo t2 = new ToDo("second", Priority.MEDIUM);
		t2.addTag(tagApple);

		list.addTodo(t1);
		list.addTodo(t2);
	}
}
