package org.taskaroo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ToDo implements Serializable {

	private static final long serialVersionUID = -3934764402050614424L;

	private final Date creationDate;
	private Priority priority;
	private final List<TodoTag> tags = new ArrayList<>();
	private String text;

	public ToDo(final String text, final Priority priority) {
		this.text = text;
		this.priority = priority;
		this.creationDate = Calendar.getInstance().getTime();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null || !(obj instanceof ToDo))
			return false;
		final ToDo other = (ToDo) obj;
		return text.equals(other.text) && priority == other.priority && this.creationDate.equals(other.creationDate);
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Priority getPriority() {
		return priority;
	}

	public List<TodoTag> getTags() {
		return tags;
	}

	public String getText() {
		return text;
	}

	@Override
	public int hashCode() {
		return this.creationDate.hashCode() + this.text.hashCode() + this.priority.hashCode();
	}

	public void setPriority(final Priority priority) {
		this.priority = priority;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public boolean addTag(final TodoTag tag) {
		return this.tags.add(tag);
	}

	public void removeTag(final TodoTag tag) {
		this.tags.remove(tag);
	}

}
