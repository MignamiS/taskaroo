package org.taskaroo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A project.
 */
public class ToDoList {

	private String name;
	private final List<ToDo> todos = new ArrayList<>();
	private final Set<TodoTag> tags = new HashSet<>();

	public ToDoList(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public boolean addTag(final TodoTag tag) {
		return this.tags.add(tag);
	}

	public void addTodo(final ToDo todo) {
		this.todos.add(todo);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(String.format("ToDo list %s (%d elements)%n", name, this.todos.size()));
		for (final ToDo item : this.todos)
			sb.append("\t" + item.toString() + "\n");
		return sb.toString();
	}
}
