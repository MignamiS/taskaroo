package org.taskaroo;

import java.io.Serializable;

/**
 * Meta-information attached to a ToDo
 */
public class TodoTag implements Serializable {

	private static final long serialVersionUID = 9020253042360385967L;

	private final String text;

	public TodoTag(final String text) {
		this.text = text;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null || !(obj instanceof TodoTag))
			return false;
		return this.text.equals(((TodoTag) obj).text);
	}

	public String getText() {
		return text;
	}

	@Override
	public int hashCode() {
		return this.text.hashCode();
	}

	@Override
	public String toString() {
		return "Tag= " + this.text;
	}
}
