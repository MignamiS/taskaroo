package todo;

public enum Category {
	TODO {
		@Override
		public int numValue() {
			return 2;
		}
	},
	BUGFIX {
		@Override
		public int numValue() {
			return 1;
		}
	},
	CURRENT {
		@Override
		public int numValue() {
			return 3;
		}
	};
	/**
	 * Return a number to compare priorities.
	 * 
	 * @return int
	 */
	public abstract int numValue();
}
