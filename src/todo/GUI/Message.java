package todo.GUI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class Message {

	public void displayInformation(Shell shell, String title, String message) {
		MessageBox info = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.OK);
		info.setText(title);
		info.setMessage(message);

		info.open();
	}

	public void displayWarning(final Shell shell, final String message) {
		final MessageBox warn = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
		warn.setText("Warning");
		warn.setMessage(message);
		warn.open();
	}

	public int displayQuestionBeforeLoading(Shell shell) {
		MessageBox question = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES
				| SWT.NO);
		question.setText("Data modified");
		question.setMessage("Save modifications before loading?");

		return question.open();
	}

	public int displayQuestionSaveBeforeExit(final Shell shell) {
		final MessageBox question = new MessageBox(shell, SWT.ICON_QUESTION
				| SWT.YES | SWT.NO);
		question.setText("Data modified");
		final String line1 = "Save modifications before exiting?";
		final String line2 = "Press YES to save and exit, press NO to exit without save";
		question.setMessage(String.format("%s%n%s", line1, line2));

		return question.open();
	}

	public void displayError(final Shell shell, final String text) {
		final MessageBox message = new MessageBox(shell, SWT.ICON_ERROR
				| SWT.OK);
		message.setText("Error");
		message.setMessage(text);
		message.open();
	}

	public void displayGuide(final Shell shell) {
		final MessageBox guide = new MessageBox(shell, SWT.ICON_INFORMATION
				| SWT.OK);
		guide.setText("Guide");
		final String line1 = "The command Archive will clear all the checked tasks and put them into a text file. The command Report will create a text file with the state of the work, prompting each task with its properties.";
		final String line2 = "The Filter menu will show only the task with the specified priority or category.";
		final String line3 = "Tip: before kicking your computer or blame me, refresh the task list. In addition,�remember to save before exiting the program.";

		guide.setMessage(String.format("%s%n%s%n%s", line1, line2, line3));
		guide.open();
	}

	public void displayAbout(final Shell shell) {
		final MessageBox about = new MessageBox(shell, SWT.ICON_INFORMATION
				| SWT.OK);
		about.setText("About");
		final String line1 = MyGUI.PROGRAM_NAME + " " + MyGUI.VERSION;
		final String line2 = "Author: Simone Mignami";
		final String line3 = "If you need some help, contact me at: simone.mignami@bluewin.ch";

		about.setMessage(String.format("%s%n%s%n%s", line1, line2, line3));
		about.open();
	}

	public int displayQuestion(final Shell shell, final String title,
			final String message) {
		final MessageBox question = new MessageBox(shell, SWT.ICON_QUESTION
				| SWT.YES | SWT.NO);
		question.setText(title);
		question.setMessage(message);

		return question.open();
	}
}
