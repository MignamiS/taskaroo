package todo.GUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.TreeItem;

import todo.Category;
import todo.PrimaryTask;
import todo.Priority;
import todo.SubTask;
import todo.Task;
import todo.TodoList;
import todo.formatters.ArchiveFormatter;
import todo.formatters.Formatter;
import todo.formatters.ReportFormatter;

import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

/**
 * Graphic Interface
 * 
 * @author Simone Mignami
 *
 */
public class MyGUI {

	public static final String PROGRAM_NAME = "Taskaroo";
	public static final String VERSION = "v0.9";

	private static final String KEY_TASK = "task";

	public static void main(String[] args) {

		Display display = new Display();
		@SuppressWarnings("unused")
		MyGUI gui = new MyGUI(display);
		display.dispose();
	}

	/* data */
	private TodoList model;
	/* text field */
	private Text textField;
	/* Priorities */
	private Combo comboPriority;

	/* categories */
	private Combo comboCategory;
	// tree
	private Tree myTree;

	private int lastItemSelected;
	private Composite composite;

	private Label lblShow;
	// filters
	private Button btnHigh;
	private Button btnMedium;
	private Button btnLow;
	private Button btnTodo;
	private Button btnBugfix;

	private Button btnCurrent;

	private Shell shell;

	public MyGUI(Display display) {
		shell = new Shell(display);
		shell.setLayout(new FillLayout());
		shell.setText(PROGRAM_NAME);
		shell.setSize(640, 480);
		shell.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent arg0) {
				// save before exit
				if (model.isDirty()) {
					int val = new Message()
							.displayQuestionSaveBeforeExit(shell);
					if (val == SWT.YES) {
						try {
							model.save();
						} catch (IOException e) {
							new Message().displayError(shell,
									"" + e.getMessage());
						}
					}
				}
			}
		});

		// init model
		TodoList todoList = new TodoList();
		try {
			todoList.load();
			this.model = todoList;
		} catch (ClassNotFoundException e) {
			new Message().displayError(shell, e.getMessage());
			System.exit(1);
		} catch (IOException e) {
			this.model = new TodoList();
			new Message().displayError(shell, "" + e.getMessage());
		}

		// init GUI
		createGUI(shell);
		populate();

		shell.open();

		// main loop
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private boolean addElement(TreeItem parent) {
		if (parent == null) {
			// get the current filter selection (better usability)
			final Priority p = getCurrentFilteredPriority();
			final Category c = getCurrentFilteredCategory();

			// create new PrimaryTask
			final PrimaryTask task = new PrimaryTask(false, null, "", c, p);
			model.addTodo(task);
			refresh();
			return true;
		} else {
			// create a SubTask
			if (parent.getData(KEY_TASK) instanceof PrimaryTask) {
				final PrimaryTask task = (PrimaryTask) parent.getData(KEY_TASK);
				task.addSubTask(new SubTask(false, null, ""));
				refresh();
				return true;
			} else {
				// a subtask is selected, find his parent
				parent = parent.getParentItem();
				if (parent.getData(KEY_TASK) instanceof PrimaryTask) {
					final PrimaryTask task = (PrimaryTask) parent
							.getData(KEY_TASK);
					task.addSubTask(new SubTask(false, null, ""));
					refresh();
				}
				return true;
			}
		}

	}

	// build the menu bar
	private void buildMenu(Shell shell, Menu bar) {
		/*
		 * File menu
		 */
		MenuItem fileMenu = new MenuItem(bar, SWT.CASCADE);
		fileMenu.setText("&File");
		Menu subMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenu.setMenu(subMenu);

		// Save
		MenuItem miSave = new MenuItem(subMenu, SWT.PUSH);
		miSave.setText("Save &ToDos\tCtrl+S");
		miSave.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				try {
					model.save();
				} catch (IOException e) {
					new Message().displayError(shell, e.getMessage());
				}
			}
		});
		miSave.setAccelerator(SWT.MOD1 + 'S');

		// Load
		MenuItem miLoad = new MenuItem(subMenu, SWT.PUSH);
		miLoad.setText("Load &ToDos\tCtrl+L");
		miLoad.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				boolean save = false;
				if (model.isDirty()) {
					int val = new Message().displayQuestionBeforeLoading(shell);
					switch (val) {
					case SWT.YES:
						save = true;
						break;
					case SWT.NO:
						save = false;
						break;
					default:
						throw new IllegalArgumentException(
								"Answer to the load question not recognized");
					}
				}

				if (save) {
					try {
						model.save();
					} catch (IOException e) {
						new Message().displayError(shell, e.getMessage());
						return;
					}
				}

				try {
					model.load();
				} catch (ClassNotFoundException e) {
					new Message().displayError(shell, e.getMessage());
					System.exit(1);
				} catch (IOException e) {
					new Message().displayError(shell, e.getMessage());
					model = new TodoList();
				}
				refresh();

				// select first element
				if (myTree.getItemCount() > 0) {
					myTree.select(myTree.getItem(0));
				}
			}
		});
		miLoad.setAccelerator(SWT.MOD1 + 'L');

		new MenuItem(subMenu, SWT.SEPARATOR);

		// refresh
		MenuItem miREfresh = new MenuItem(subMenu, SWT.PUSH);
		miREfresh.setText("Refresh\tF5");
		miREfresh.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				refresh();
				System.out.println("Tree refreshed");
			}
		});
		miREfresh.setAccelerator(SWT.F5);

		// archive dove
		MenuItem miArchive = new MenuItem(subMenu, SWT.PUSH);
		miArchive.setText("Archive &done");
		miArchive.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// format all done task
				List<String> list = Formatter.formatForArchiviation(model,
						new ArchiveFormatter(), true);
				if (list.size() > 0) {
					// print on file
					try {
						model.archive(list);
					} catch (IOException e) {
						new Message().displayError(shell, "" + e.getMessage());
						return;
					}
					// delete all done task from model
					model.deleteAllDone();
					refresh();
					// ask for save
					int val = new Message()
							.displayQuestion(
									shell,
									"Save modification",
									"The task list has been modified, do you want to save it before you lose something?");
					if (val == SWT.YES) {
						try {
							model.save();
						} catch (IOException e) {
							new Message().displayError(shell,
									"" + e.getMessage());
							return;
						}
					}

				} else {
					new Message().displayWarning(shell,
							"There are not done task to archive!");
				}
			}
		});

		// print report
		MenuItem miReport = new MenuItem(subMenu, SWT.PUSH);
		miReport.setText("Print report");
		miReport.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				List<String> toPrint = Formatter.formatForReport(model,
						new ReportFormatter());
				try {
					model.report(toPrint);
				} catch (IOException e) {
					new Message().displayError(shell, "" + e.getMessage());
					return;
				}
				new Message().displayInformation(shell, "Information",
						"The report has been successfully generated");
			}
		});

		new MenuItem(subMenu, SWT.SEPARATOR);

		// quit
		MenuItem miQuit = new MenuItem(subMenu, SWT.PUSH);
		miQuit.setText("Quit");
		miQuit.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				quit();
			}
		});

		/*
		 * Todos menu
		 */
		MenuItem TodosMenu = new MenuItem(bar, SWT.CASCADE);
		TodosMenu.setText("&ToDos");
		subMenu = new Menu(shell, SWT.DROP_DOWN);
		TodosMenu.setMenu(subMenu);

		// Create Task
		MenuItem miCreate = new MenuItem(subMenu, SWT.PUSH);
		miCreate.setText("Create &Task\tCtrl+N");
		miCreate.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// remove focus from the text field to save data
				if (textField.isFocusControl())
					myTree.setFocus();

				addElement(null);
				// look for the element and select it
				for (int i = 0; i < myTree.getItemCount(); i++) {
					TreeItem item = myTree.getItem(i);
					if (item.getText().equals(""))
						myTree.select(item);
				}
				refreshFormField();
			}
		});
		miCreate.setAccelerator(SWT.MOD1 + 'N');

		// Create Subtask
		MenuItem miCreateSub = new MenuItem(subMenu, SWT.PUSH);
		miCreateSub.setText("Create &SubTask\tCtrl+M");
		miCreateSub.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				TreeItem[] selection = myTree.getSelection();
				if (selection.length > 0) {
					/*
					 * Set the last selected item using the primary task
					 * position. If a Sub-task is selected, his parent will be
					 * take in account.
					 */
					if (selection[0].getData(KEY_TASK) instanceof PrimaryTask)
						lastItemSelected = myTree.indexOf(selection[0]);
					else if (selection[0].getData(KEY_TASK) instanceof SubTask)
						lastItemSelected = myTree.indexOf(selection[0]
								.getParentItem());

					// save text field data before creating new sub-element
					if (textField.isFocusControl())
						myTree.setFocus();

					if (addElement(selection[0])) {
						TreeItem selected = myTree.getItem(lastItemSelected);
						myTree.select(selected);
						selected.setExpanded(true);
						lastItemSelected = -1;
						model.setDirty(true);
					}
				} else {
					new Message().displayError(shell, "No element selected");
				}
				refreshFormField();
			}
		});
		miCreateSub.setAccelerator(SWT.MOD1 + 'M');

		new MenuItem(subMenu, SWT.SEPARATOR);

		// Make Current
		MenuItem miSetCurrent = new MenuItem(subMenu, SWT.PUSH);
		miSetCurrent.setText("Make &selected &Current\tCtrl+C");
		miSetCurrent.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// select element from tree
				TreeItem[] selection = myTree.getSelection();
				if (selection.length > 0) {
					Object tmp = selection[0].getData(KEY_TASK);
					if (tmp instanceof PrimaryTask) {
						PrimaryTask data = (PrimaryTask) tmp;
						if (data.getCategory() == Category.CURRENT)
							data.setCategory(Category.TODO);
						else
							data.setCategory(Category.CURRENT);

						model.setDirty(true);
					}
				}
				refresh();
			}
		});
		miSetCurrent.setAccelerator(SWT.MOD1 + 'C');

		// expand all
		MenuItem miExpand = new MenuItem(subMenu, SWT.PUSH);
		miExpand.setText("Expand &all");
		miExpand.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				expandAll(true);
			}

		});
		miExpand.setAccelerator(SWT.MOD1 + 'E');

		// collapse all
		MenuItem miCollapse = new MenuItem(subMenu, SWT.PUSH);
		miCollapse.setText("Collapse &all");
		miCollapse.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				expandAll(false);
			}
		});

		new MenuItem(subMenu, SWT.SEPARATOR);

		// Delete
		MenuItem miDelete = new MenuItem(subMenu, SWT.PUSH);
		miDelete.setText("Delete\tDel");
		miDelete.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				TreeItem[] sel = myTree.getSelection();
				if (sel.length > 0) {
					// save position and remove item
					TreeItem itemSelected = sel[0];
					lastItemSelected = myTree.indexOf(itemSelected);
					removeElement(itemSelected);
					// jump the selection on the last position
					if (lastItemSelected >= 0
							&& lastItemSelected < myTree.getItemCount()) {
						myTree.select(myTree.getItem(lastItemSelected));
					}
				}
			}
		});
		miDelete.setAccelerator(SWT.DEL);

		/*
		 * Filter menu
		 */
		MenuItem filterMenu = new MenuItem(bar, SWT.CASCADE);
		filterMenu.setText("&Filter");
		subMenu = new Menu(shell, SWT.DROP_DOWN);
		filterMenu.setMenu(subMenu);

		// All
		MenuItem miAll = new MenuItem(subMenu, SWT.PUSH);
		miAll.setText("Show &All\tF1");
		miAll.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// check all button
				btnHigh.setSelection(true);
				btnMedium.setSelection(true);
				btnLow.setSelection(true);
				btnCurrent.setSelection(true);
				btnTodo.setSelection(true);
				btnBugfix.setSelection(true);

				refresh();
				System.out.println("All filter enabled");
			}
		});
		miAll.setAccelerator(SWT.F1);

		// todo only
		MenuItem miTodo = new MenuItem(subMenu, SWT.PUSH);
		miTodo.setText("Show &ToDo\tF2");
		miTodo.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// check todo only
				btnTodo.setSelection(true);
				btnBugfix.setSelection(false);
				btnCurrent.setSelection(false);

				refresh();
				System.out.println("Todo selected");
			}
		});
		miTodo.setAccelerator(SWT.F2);

		// bugfix only
		MenuItem miBug = new MenuItem(subMenu, SWT.PUSH);
		miBug.setText("Show &BugFix\tF3");
		miBug.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// select only bugfix
				btnTodo.setSelection(false);
				btnBugfix.setSelection(true);
				btnCurrent.setSelection(false);

				refresh();
				System.out.println("BugFix selected");
			}
		});
		miBug.setAccelerator(SWT.F3);

		new MenuItem(subMenu, SWT.SEPARATOR);

		// high only
		MenuItem miHigh = new MenuItem(subMenu, SWT.PUSH);
		miHigh.setText("Show &High &only\tCtrl+1");
		miHigh.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// check only high priority
				btnHigh.setSelection(true);
				btnMedium.setSelection(false);
				btnLow.setSelection(false);

				refresh();
			}
		});
		miHigh.setAccelerator(SWT.MOD1 + '1');

		// medium only
		MenuItem miMed = new MenuItem(subMenu, SWT.PUSH);
		miMed.setText("Show &Medium &only\tCtrl+2");
		miMed.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// check only medium priority
				btnHigh.setSelection(false);
				btnMedium.setSelection(true);
				btnLow.setSelection(false);

				refresh();
			}
		});
		miMed.setAccelerator(SWT.MOD1 + '2');

		// low only
		MenuItem miLow = new MenuItem(subMenu, SWT.PUSH);
		miLow.setText("Show &Low &only\tCtrl+3");
		miLow.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// check only high priority
				btnHigh.setSelection(false);
				btnMedium.setSelection(false);
				btnLow.setSelection(true);

				refresh();
			}
		});
		miLow.setAccelerator(SWT.MOD1 + '3');

		// Current only
		MenuItem miCur = new MenuItem(subMenu, SWT.PUSH);
		miCur.setText("Show &Current &only\tF4");
		miCur.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// check only high priority
				btnCurrent.setSelection(true);
				btnTodo.setSelection(false);
				btnBugfix.setSelection(false);

				refresh();
			}
		});
		miCur.setAccelerator(SWT.F4);

		/*
		 * Help
		 */
		MenuItem helpMenu = new MenuItem(bar, SWT.CASCADE);
		helpMenu.setText("&help");
		subMenu = new Menu(shell, SWT.DROP_DOWN);
		helpMenu.setMenu(subMenu);

		// Guide
		MenuItem miGuide = new MenuItem(subMenu, SWT.PUSH);
		miGuide.setText("Guide");
		miGuide.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				new Message().displayGuide(shell);
			}
		});

		// About
		MenuItem miAbout = new MenuItem(subMenu, SWT.PUSH);
		miAbout.setText("About me");
		miAbout.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				new Message().displayAbout(shell);
			}
		});

	}

	@PostConstruct
	public void createGUI(Composite parent) {
		parent.setLayout(new GridLayout(1, false));

		// menu bar
		Shell shell = (Shell) parent;
		Menu bar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(bar);

		buildMenu(shell, bar);

		myTree = new Tree(parent, SWT.BORDER | SWT.CHECK);
		myTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		myTree.addSelectionListener(new SelectionAdapter() {
			private TreeItem currentTreeItem;

			@Override
			public void widgetSelected(SelectionEvent event) {
				// If the item is changed, update the form fields
				boolean changed = false;
				if (currentTreeItem == null) {

					currentTreeItem = (TreeItem) event.item;
					changed = true;
				} else if (!((TreeItem) event.item).equals(currentTreeItem)) {
					currentTreeItem = (TreeItem) event.item;
					changed = true;
				}

				// check status
				if (event.detail == SWT.CHECK) {
					boolean check = currentTreeItem.getChecked();
					Task tmp = (Task) currentTreeItem.getData(KEY_TASK);
					tmp.setDone(check);
					model.setDirty(true);
				}

				// update form field
				if (changed) {
					refreshFormField();
				}
			}
		});

		Composite compositeForm = new Composite(parent, SWT.BORDER);
		GridData gd_compositeForm = new GridData(SWT.LEFT, SWT.CENTER, true,
				false, 1, 1);
		gd_compositeForm.widthHint = 539;
		compositeForm.setLayoutData(gd_compositeForm);
		compositeForm.setLayout(new GridLayout(2, false));

		Label lblDescription = new Label(compositeForm, SWT.BORDER);
		lblDescription.setText("Description");

		textField = new Text(compositeForm, SWT.BORDER);
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.keyCode == SWT.CR) {
					myTree.setFocus();
				}
			}
		});
		textField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if (textField.getText() != null
						&& !textField.getText().equals("")) {
					textField.selectAll();
				}
			}

			@Override
			public void focusLost(FocusEvent event) {
				TreeItem[] selection = myTree.getSelection();
				if (selection.length > 0) {
					TreeItem currentTreeItem = selection[0];
					currentTreeItem.setText(textField.getText());
					Task data = (Task) currentTreeItem.getData(KEY_TASK);
					data.setText(textField.getText());
					model.setDirty(true);
				}
			}
		});
		textField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));
		textField.setText("");

		Label lblPriority = new Label(compositeForm, SWT.NONE);
		lblPriority.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblPriority.setText("Priority");

		comboPriority = new Combo(compositeForm, SWT.READ_ONLY);
		comboPriority.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.keyCode == SWT.CR)
					myTree.setFocus();
			}
		});
		comboPriority.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent event) {
				TreeItem[] selection = myTree.getSelection();
				if (selection.length > 0) {
					TreeItem currentTreeItem = selection[0];
					PrimaryTask data = (PrimaryTask) currentTreeItem
							.getData(KEY_TASK);
					data.setPriority(Priority.valueOf(comboPriority.getText()));
					model.setDirty(true);
				}
			}
		});
		comboPriority.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		for (Priority item : Priority.values()) {
			comboPriority.add(item.toString());
		}
		comboPriority.setText(TodoList.DEFAULT_PRIORITY.toString());

		Label lblCategory = new Label(compositeForm, SWT.NONE);
		lblCategory.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblCategory.setText("Category");

		comboCategory = new Combo(compositeForm, SWT.READ_ONLY);
		comboCategory.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.keyCode == SWT.CR)
					myTree.setFocus();
			}
		});
		comboCategory.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent event) {
				TreeItem[] selection = myTree.getSelection();
				if (selection.length > 0) {
					TreeItem currentTreeItem = selection[0];
					PrimaryTask data = (PrimaryTask) currentTreeItem
							.getData(KEY_TASK);
					data.setCategory(Category.valueOf(comboCategory.getText()));
					model.setDirty(true);
				}
			}
		});
		comboCategory.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		for (Category item : Category.values()) {
			comboCategory.add(item.toString());
		}
		comboCategory.setText(TodoList.DEFAULT_CATEGORY.toString());

		composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new RowLayout(SWT.HORIZONTAL));

		lblShow = new Label(composite, SWT.NONE);
		lblShow.setText("Show:   ");

		btnHigh = new Button(composite, SWT.CHECK);
		btnHigh.setSelection(true);
		btnHigh.setText("High");

		btnMedium = new Button(composite, SWT.CHECK);
		btnMedium.setSelection(true);
		btnMedium.setText("Medium");

		btnLow = new Button(composite, SWT.CHECK);
		btnLow.setSelection(true);
		btnLow.setText("Low");

		btnTodo = new Button(composite, SWT.CHECK);
		btnTodo.setSelection(true);
		btnTodo.setText("ToDo");

		btnBugfix = new Button(composite, SWT.CHECK);
		btnBugfix.setSelection(true);
		btnBugfix.setText("BugFix");

		btnCurrent = new Button(composite, SWT.CHECK);
		btnCurrent.setSelection(true);
		btnCurrent.setText("Current");
	}

	/*
	 * Filter the model and return a sub-list of task in accord whit the GUI
	 */
	private List<PrimaryTask> filter() {
		List<PrimaryTask> source = model.getTodo();
		List<PrimaryTask> temp1 = new ArrayList<>();
		// filter Category
		for (PrimaryTask task : source) {
			if (btnTodo.getSelection() && task.getCategory() == Category.TODO)
				temp1.add(task);
			else if (btnBugfix.getSelection()
					&& task.getCategory() == Category.BUGFIX)
				temp1.add(task);
			else if (btnCurrent.getSelection()
					&& task.getCategory() == Category.CURRENT)
				temp1.add(task);
		}

		// filter priority
		List<PrimaryTask> dest = new ArrayList<>();
		for (PrimaryTask task : temp1) {
			if (btnHigh.getSelection() && task.getPriority() == Priority.HIGH)
				dest.add(task);
			else if (btnMedium.getSelection()
					&& task.getPriority() == Priority.MEDIUM)
				dest.add(task);
			else if (btnLow.getSelection()
					&& task.getPriority() == Priority.LOW)
				dest.add(task);
		}

		return dest;
	}

	// return the priority or the default one
	private Category getCurrentFilteredCategory() {
		final boolean t = btnTodo.getSelection();
		final boolean b = btnBugfix.getSelection();
		final boolean c = btnCurrent.getSelection();

		if ((t & b) || (t & c) || (b & c)) {
			// return default
			return TodoList.DEFAULT_CATEGORY;
		}

		if (t)
			return Category.TODO;
		else if (b)
			return Category.BUGFIX;
		else if (c)
			return Category.CURRENT;
		else
			return TodoList.DEFAULT_CATEGORY;
	}

	// return the priority or the default one
	private Priority getCurrentFilteredPriority() {
		final boolean h = btnHigh.getSelection();
		final boolean m = btnMedium.getSelection();
		final boolean l = btnLow.getSelection();

		if ((h & m) || (m & l) || (h & l)) {
			// more than a filter are active
			return TodoList.DEFAULT_PRIORITY;
		}

		if (h)
			return Priority.HIGH;
		else if (m)
			return Priority.MEDIUM;
		else if (l)
			return Priority.LOW;
		else
			return TodoList.DEFAULT_PRIORITY;
	}

	// insert data model into the tree
	private void populate() {
		// filter and order task
		List<PrimaryTask> tasks = filter();

		Collections.sort(tasks);
		for (final PrimaryTask task : tasks) {
			final TreeItem item = new TreeItem(myTree, SWT.NONE);
			item.setText(task.getText());
			item.setChecked(task.isDone());
			item.setData(KEY_TASK, task);
			// subitems
			if (task.getSubTasks().size() > 0) {
				for (final SubTask subTask : task.getSubTasks()) {
					final TreeItem subItem = new TreeItem(item, SWT.NONE);
					subItem.setText(subTask.getText());
					subItem.setChecked(subTask.isDone());
					subItem.setData(KEY_TASK, subTask);

				}
			}

		}
	}

	private void quit() {
		this.shell.dispose();
	}

	// update the tree
	private void refresh() {
		myTree.removeAll();
		populate();
		selectFirst();
		refreshFormField();
	}

	private void refreshFormField() {
		TreeItem[] selection = myTree.getSelection();
		if (selection.length > 0) {
			Task item = (Task) selection[0].getData(KEY_TASK);
			if (item instanceof PrimaryTask) {
				PrimaryTask task = (PrimaryTask) item;
				textField.setText(task.getText());
				comboPriority.setText(task.getPriority().toString());
				comboPriority.setEnabled(true);
				comboCategory.setText(task.getCategory().toString());
				comboCategory.setEnabled(true);
			} else if (item instanceof SubTask) {
				SubTask task = (SubTask) item;
				textField.setText(task.getText());
				comboPriority.setEnabled(false);
				comboCategory.setEnabled(false);
			}
		}
	}

	private void removeElement(TreeItem sel) {
		Task task = (Task) sel.getData(KEY_TASK);
		if (task instanceof PrimaryTask) {
			// delete the entire task (primary)
			model.removeTodo((PrimaryTask) task);
			refresh();
		} else if (task instanceof SubTask) {
			// look for the parent and delete the subtask from it
			TreeItem parent = sel.getParentItem();
			if (parent == null) {
				System.out.println("something goes wrong...");
			} else {
				((PrimaryTask) parent.getData(KEY_TASK)).getSubTasks().remove(
						task);
				refresh();
			}
		}
	}

	// select first element of the tree
	private void selectFirst() {
		if (myTree.getItemCount() > 0) {
			myTree.select(myTree.getItem(0));
		}
	}

	/**
	 * Expand or collapse all the TreeItems
	 * 
	 * @param expand
	 *            TRUE or FALSE to collapse all
	 */
	private void expandAll(boolean expand) {
		for (TreeItem item : myTree.getItems()) {
			item.setExpanded(expand);
		}
	}
}
