package todo;

import java.util.ArrayList;
import java.util.List;

public class PrimaryTask extends Task implements Comparable<PrimaryTask> {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 2040111009920763889L;

	private Priority priority;

	private Category category;
	private final List<SubTask> subTasks = new ArrayList<>();

	public PrimaryTask(final boolean done, final String doneTime,
			final String text, final Category category, final Priority priority) {
		super(done, doneTime, text);
		this.priority = priority;
		this.category = category;
	}

	public void addSubTask(SubTask task) {
		this.subTasks.add(task);
	}

	@Override
	public int compareTo(PrimaryTask other) {
		// check category, the priority and lastly string alphabetic order
		if (this.category != other.category)
			return other.category.numValue() - this.category.numValue();
		else if (this.priority != other.priority)
			return other.priority.numValue() - this.priority.numValue();
		else
			return this.getText().compareTo(other.getText());
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof PrimaryTask))
			return false;

		PrimaryTask o = (PrimaryTask) other;
		if (o.getCategory() == this.category
				&& o.getPriority() == this.priority
				&& o.getText().equals(this.getText()))
			return true;
		else
			return false;
	}

	public Category getCategory() {
		return category;
	}

	public Priority getPriority() {
		return priority;
	}

	public List<SubTask> getSubTasks() {
		return subTasks;
	}

	public void removeSubTask(final SubTask task) {
		this.subTasks.remove(task);
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		final int size = this.subTasks.size();
		return String
				.format("Primary task: %s (Category=%s, Priority=%s) with %d subtask(s)",
						this.getText(), this.getCategory(), this.getPriority(),
						size);
	}
}