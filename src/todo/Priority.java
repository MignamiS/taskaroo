package todo;

public enum Priority {
	HIGH {
		@Override
		public int numValue() {
			return 3;
		}
	},
	MEDIUM {
		@Override
		public int numValue() {
			return 2;
		}
	},
	LOW {
		@Override
		public int numValue() {
			return 1;
		}
	};

	/**
	 * Return the numerical value for a comparison.
	 * 
	 * @return int
	 */
	public abstract int numValue();
}
