package todo;

public class SubTask extends Task {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 23846459087111001L;

	public SubTask(boolean done, String doneTime, String text) {
		super(done, doneTime, text);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof SubTask))
			return false;

		SubTask o = (SubTask) other;
		if (o.getText().equals(this.getText()))
			return true;
		else
			return false;
	}

	@Override
	public String toString() {
		return "Subtask: " + this.getText();
	}

}
