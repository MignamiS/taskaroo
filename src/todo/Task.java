package todo;

import java.io.Serializable;
import java.time.LocalDateTime;

public abstract class Task implements Serializable {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 2528863608570455908L;

	private boolean done;
	private String doneTime;

	private String text;

	public Task(final boolean done, final String text) {
		this.done = done;
		this.text = text;
	}

	public Task(final boolean done, final String doneTime, final String text) {
		this.done = done;
		this.doneTime = doneTime;
		this.text = text;
	}

	public String getDoneTime() {
		return doneTime;
	}

	public String getText() {
		return text;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		/*
		 * If the done-state change from FALSE to TRUE, generate a string with
		 * the current datetime.
		 */
		if (this.done == false && done == true) {
			final LocalDateTime time = LocalDateTime.now();
			final int year = time.getYear();
			final int mon = time.getMonthValue();
			final int day = time.getDayOfMonth();
			final int h = time.getHour();
			final int m = time.getMinute();
			this.doneTime = String.format("%d:%02d:%02d-%02d:%02d", year, mon,
					day, h, m);
		}
		this.done = done;
	}

	public void setDoneTime(String doneTime) {
		this.doneTime = doneTime;
	}

	public void setText(String text) {
		this.text = text;
	}

}
