package todo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import todo.formatters.ArchiveFormatter;
import todo.formatters.ReportFormatter;

public class TodoList {

	public static final Priority DEFAULT_PRIORITY = Priority.MEDIUM;
	public static final Category DEFAULT_CATEGORY = Category.TODO;

	private static final String DATA_FILENAME = "todo_data";
	private static final String ARCHIVE_FILENAME = "archived.csv";
	private static final String REPORT_FILENAME = "report";
	private static final String REPORT_FILE_EXTENSION = ".txt";

	private List<PrimaryTask> todo = new ArrayList<>();
	// indicates whether the todo list has been modified
	private boolean dirty = false;

	public void addTodo(PrimaryTask task) {
		this.todo.add(task);
		this.dirty = true;
	}

	public List<PrimaryTask> getTodo() {
		return todo;
	}

	public boolean isDirty() {
		return dirty;
	}

	@SuppressWarnings("unchecked")
	public void load() throws IOException, ClassNotFoundException {
		// create file if not exists
		File file = new File(DATA_FILENAME);
		if (!file.exists()) {
			file.createNewFile();
			this.todo = new ArrayList<>();
			// save an empty list
			this.dirty = true;
			this.save();
			return;
		}

		FileInputStream fileIn = new FileInputStream(DATA_FILENAME);
		ObjectInputStream in = new ObjectInputStream(fileIn);
		this.todo = (List<PrimaryTask>) in.readObject();
		in.close();
		fileIn.close();
		this.dirty = false;
	}

	public void removeTodo(PrimaryTask task) {
		this.todo.remove(task);
		this.dirty = true;
	}

	public void save() throws IOException {
		if (this.dirty) {
			FileOutputStream fileOut = new FileOutputStream(DATA_FILENAME);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(this.todo);
			out.close();
			fileOut.close();
			this.dirty = false;
			System.out.println("data saved");
		} else {
			System.out.println("there is nothin to save");
		}
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	public void archive(List<String> lines) throws IOException {
		File file = new File(ARCHIVE_FILENAME);
		printOnFile(lines, file, new ArchiveFormatter().generateHeader(), true);
	}

	public void report(List<String> lines) throws IOException {
		LocalDateTime time = LocalDateTime.now();
		int min = time.getMinute();
		int hours = time.getHour();
		int day = time.getDayOfMonth();
		int mon = time.getMonthValue();
		int year = time.getYear();
		String date = String.format("%d_%02d_%02d_%02d%02d", year, mon, day,
				hours, min);

		File file = new File(REPORT_FILENAME + "_" + date
				+ REPORT_FILE_EXTENSION);
		printOnFile(lines, file, new ReportFormatter().generateHeader(), false);
	}

	private void printOnFile(final List<String> lines, final File file,
			final String header, final boolean append) throws IOException {

		// check if the file exists and, if not, initialize with header
		boolean fresh = false;
		if (!file.exists()) {
			file.createNewFile();
			fresh = true;
		}

		// append on file
		final PrintWriter writer = new PrintWriter(new BufferedWriter(
				new FileWriter(file, append)));
		// header
		if (fresh || !append) {
			writer.println(header);
			writer.println();
		}

		for (final String item : lines) {
			writer.println(item);
		}

		writer.flush();
		writer.close();
	}

	public void deleteAllDone() {
		ArrayList<PrimaryTask> toDelete = new ArrayList<>();
		// pick all done
		for (final PrimaryTask task : this.todo) {
			if (task.isDone())
				toDelete.add(task);
		}

		if (toDelete.size() > 0) {
			this.todo.removeAll(toDelete);
			this.dirty = true;
		}
	}

}
