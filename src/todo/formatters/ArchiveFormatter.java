package todo.formatters;

import todo.PrimaryTask;
import todo.SubTask;

public class ArchiveFormatter implements ITodoFormatter {

	@Override
	public String format(final PrimaryTask task) {
		final char tab = '\t';
		final StringBuilder sb = new StringBuilder();
		sb.append("" + task.getDoneTime());
		sb.append(tab);
		sb.append(task.getCategory().toString());
		sb.append(tab);
		sb.append(task.getPriority().toString());
		sb.append(tab);
		sb.append(task.getText());
		sb.append(tab);

		// sub tasks
		for (int i = 0; i < task.getSubTasks().size(); i++) {
			final SubTask st = task.getSubTasks().get(i);
			// add comma
			if (i > 0)
				sb.append(String.format(", %s (%s)", st.getText(),
						st.getDoneTime()));
			else
				sb.append(String.format("%s (%s)", st.getText(),
						st.getDoneTime()));
		}

		return sb.toString();
	}

	@Override
	public String generateHeader() {
		return "Done on\tCategory\tPriority\tText\tSubtask(s)";
	}

}
