package todo.formatters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import todo.Category;
import todo.PrimaryTask;
import todo.Priority;
import todo.TodoList;

public class Formatter {

	public static List<String> formatForReport(final TodoList todoList,
			final ITodoFormatter formatter) {
		final List<String> toPrint = new ArrayList<>();
		// collect pending todos
		final List<PrimaryTask> tmpTodo = todoList
				.getTodo()
				.stream()
				.filter(item -> {
					return item.getCategory() == Category.TODO
							&& !item.isDone();
				}).collect(Collectors.toList());
		// collect pending bugfix
		final List<PrimaryTask> tmpBugFix = todoList
				.getTodo()
				.stream()
				.filter(item -> {
					return item.getCategory() == Category.BUGFIX
							&& !item.isDone();
				}).collect(Collectors.toList());
		// collect pending Current task
		final List<PrimaryTask> tmpCurrent = todoList
				.getTodo()
				.stream()
				.filter(item -> {
					return item.getCategory() == Category.CURRENT
							&& !item.isDone();
				}).collect(Collectors.toList());

		// print current tasks sorted by priority
		if (tmpCurrent.size() > 0) {
			toPrint.add(ReportFormatter.generateTitle(
					ReportFormatter.SIMPLE_TITLE_1, Category.CURRENT.toString()));
			Collections.sort(tmpCurrent);
			tmpCurrent.stream().forEach(item -> {
				toPrint.add(formatter.format(item));
			});
			toPrint.add("");
			toPrint.add("");
		}

		// print todos
		toPrint.add(ReportFormatter.generateTitle(
				ReportFormatter.SIMPLE_TITLE_1, Category.TODO.toString()));
		toPrint.add(ReportFormatter.generateTitle(
				ReportFormatter.SIMPLE_TITLE_2, Priority.HIGH.toString()));
		tmpTodo.stream().forEach(item -> {
			if (item.getPriority() == Priority.HIGH)
				toPrint.add(formatter.format(item));
		});
		toPrint.add("");
		toPrint.add(ReportFormatter.generateTitle(
				ReportFormatter.SIMPLE_TITLE_2, Priority.MEDIUM.toString()));
		tmpTodo.stream().forEach(item -> {
			if (item.getPriority() == Priority.MEDIUM)
				toPrint.add(formatter.format(item));
		});
		toPrint.add("");
		toPrint.add(ReportFormatter.generateTitle(
				ReportFormatter.SIMPLE_TITLE_2, Priority.LOW.toString()));
		tmpTodo.stream().forEach(item -> {
			if (item.getPriority() == Priority.LOW)
				toPrint.add(formatter.format(item));
		});
		toPrint.add("");
		toPrint.add("");

		// print bugfix sorted by priority
		if (tmpBugFix.size() > 0) {
			toPrint.add(ReportFormatter.generateTitle(
					ReportFormatter.SIMPLE_TITLE_1, Category.BUGFIX.toString()));
			Collections.sort(tmpBugFix);
			tmpBugFix.stream().forEach(item -> {
				toPrint.add(formatter.format(item));
			});
			toPrint.add("");
		}

		return toPrint;
	}

	public static List<String> formatForArchiviation(final TodoList todoList,
			final ITodoFormatter formatter, final boolean onlyDone) {
		final List<String> list = new ArrayList<String>();
		for (final PrimaryTask task : todoList.getTodo()) {
			if (task.isDone())
				list.add(formatter.format(task));
		}

		return list;
	}
}
