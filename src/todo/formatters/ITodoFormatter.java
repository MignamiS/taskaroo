package todo.formatters;

import todo.PrimaryTask;

public interface ITodoFormatter {
	public String format(PrimaryTask task);

	public String generateHeader();
}
