package todo.formatters;

import java.time.LocalDateTime;

import todo.PrimaryTask;

public class ReportFormatter implements ITodoFormatter {

	public static final int SIMPLE_TITLE_1 = 1;
	public static final int SIMPLE_TITLE_2 = 2;
	public static final int MAIN_TITLE = 3;

	@Override
	public String format(final PrimaryTask task) {
		final StringBuilder sb = new StringBuilder();
		sb.append("- " + task.getText());
		task.getSubTasks().forEach(item -> {
			sb.append('\n');
			sb.append("\t- " + item.getText());
		});

		// add an extra space to subdivide, in case there are more than a single
		// task
		if (task.getSubTasks().size() > 0)
			sb.append('\n');

		return sb.toString();
	}

	@Override
	public String generateHeader() {
		final LocalDateTime time = LocalDateTime.now();
		final int min = time.getMinute();
		final int hours = time.getHour();
		final int day = time.getDayOfMonth();
		final int month = time.getMonthValue();
		final int year = time.getYear();

		return generateTitle(MAIN_TITLE, String.format(
				"ToDo Report\t(Generated on %02d:%02d:%d at %02d:%02d)", day,
				month, year, hours, min));
	}

	/**
	 * Generate a multi-line string for a title, using a givven underlining
	 * 
	 * @param type
	 *            SIMPLE_TITLE_1 = title with "=", SIMPLE_TITLE_2 = title with
	 *            "-", MAIN_TITLE = main title with "="
	 * @param text
	 *            the text to be underlined
	 * @return multi-line title
	 */
	public static String generateTitle(int type, String text) {
		// underline
		char c = ' '; // used as line
		int extra = 0;// number of characters after the end of text
		// if true the line is repeated also on top of the text
		boolean top = false;

		switch (type) {
		case 1:
			c = '=';
			extra = 2;
			break;
		case 2:
			c = '-';
			extra = 1;
			break;
		case 3:
			c = '=';
			extra = 3;
			top = true;
			break;
		}

		StringBuilder sb = new StringBuilder();

		// top line
		if (top) {
			for (int i = 0; i < text.length() + extra; i++)
				sb.append(c);
			sb.append('\n');
		}

		// title text
		sb.append(text);

		sb.append('\n');
		// underline
		for (int i = 0; i < text.length() + extra; i++)
			sb.append(c);

		return sb.toString();
	}
}